# Cayenne LPP

This module is for encoding and decoding Cayenne LPP payloads. See https://github.com/myDevicesIoT/cayenne-docs/blob/master/docs/LORA.md for more details.

## Installation

```
$ npm install cayenne-lpp
```

## Usage

### Encoder

```javascript
const { encoder } = require('cayenne-lpp');

const exampleTemperature = function exampleTemperature() {
  const data = Buffer.concat([
    encoder.encodeTemperature(3, 27.2),
    encoder.encodeTemperature(5, 25.5),
  ]);
  console.log(`Device with 2 temperature sensors: ${data.toString('hex')}`);
};

const exampleTemperatureAccelerometer = function exampleTemperatureAccelerometer() {
  console.log('Device with temperature and acceleration sensors');
  const temperature = encoder.encodeTemperature(1, -4.1);
  console.log(`- Frame N: ${temperature.toString('hex')}`);
  const accelerometer = encoder.encodeAccelerometer(6, {
    x: 1.234,
    y: -1.234,
    z: 0.0,
  });
  console.log(`- Frame N+1: ${accelerometer.toString('hex')}`);
};

const exampleGps = function exampleGps() {
  const gps = encoder.encodeGps(1, {
    latitude: 42.3519,
    longitude: -87.9094,
    altitude: 10.0,
  });
  console.log(`Device with GPS: ${gps.toString('hex')}`);
};

exampleTemperature();
exampleTemperatureAccelerometer();
exampleGps();

```

### Decoder

```javascript
const { decoder } = require('cayenne-lpp');

const exampleTemperature = function exampleTemperature() {
  const buffer = Buffer.from('03670110056700ff', 'hex');
  const json = JSON.stringify(decoder.decode(buffer));
  console.log(`Device with 2 temperature sensors: ${json}}`);
};

const exampleTemperatureAccelerometer = function exampleTemperatureAccelerometer() {
  const buffer = Buffer.from('0167ffd7067104d2fb2e0000', 'hex');
  const json = JSON.stringify(decoder.decode(buffer));
  console.log(`Device with temperature and acceleration sensors: ${json}}`);
};

const exampleGPS = function exampleGPS() {
  const buffer = Buffer.from('018806765ff2960a0003e8', 'hex');
  const json = JSON.stringify(decoder.decode(buffer));
  console.log(`Device with GPS: ${json}}`);
};

exampleTemperature();
exampleTemperatureAccelerometer();
exampleGPS();
```
