/* eslint-env mocha */

const { expect } = require('chai');
const { encoder, Ranges } = require('../src');

const testUnsignedRange = function testUnsignedRange(positive, min, max, cb) {
  it(`should encode a positive value between ${min} and ${max}`, () => {
    const { channel, value, expected } = positive;
    const buffer = cb(channel, value);
    expect(buffer.toString('hex')).to.equal(expected);
  });
  it(`should throw RangeError on a value less than ${min}`, () => {
    const { channel } = positive;
    const value = min - 1;
    expect(() => cb(channel, value)).to.throw(RangeError);
  });
  it(`should throw RangeError on a value greater than ${max}`, () => {
    const { channel } = positive;
    const value = max + 1;
    expect(() => cb(channel, value)).to.throw(RangeError);
  });
};

const testSignedRange = function testSignedRange(
  negative,
  positive,
  min,
  max,
  cb,
) {
  it(`should encode a negative value between ${min} and ${max}`, () => {
    const { channel, value, expected } = negative;
    const buffer = cb(channel, value);
    expect(buffer.toString('hex')).to.equal(expected);
  });
  testUnsignedRange(positive, min, max, cb);
};

const test3AxisRange = function test3AxisRange(negative, positive, ranges, cb) {
  Object.keys(ranges).forEach((axis) => {
    describe(`${axis}`, () => {
      const [min, max] = ranges[axis];
      it(`should encode a negative value between ${min} and ${max}.`, () => {
        const { channel, values, expected } = negative[axis];
        const buffer = cb(channel, values);
        expect(buffer.toString('hex')).to.equal(expected);
      });

      it(`should encode a positive value between ${min} and ${max}.`, () => {
        const { channel, values, expected } = positive[axis];
        const buffer = cb(channel, values);
        expect(buffer.toString('hex')).to.equal(expected);
      });

      it(`should throw RangeError on value less than ${min}`, () => {
        const { channel, values } = negative[axis];
        values[axis] = min - 1;

        expect(() => cb(channel, values)).to.throw(RangeError);
      });
      it(`should throw RangeError on value greater than ${max}`, () => {
        const { channel, values } = positive[axis];
        values[axis] = max + 1;
        expect(() => cb(channel, values)).to.throw(RangeError);
      });
    });
  });
};

const testDigitalInput = function testDigitalInput() {
  const [min, max] = Ranges.DIGITAL_INPUT;
  const positive = { channel: 1, value: 200, expected: '0100c8' };
  testUnsignedRange(positive, min, max, encoder.encodeDigitalInput);
};

const testDigitalOutput = function testDigitalOutput() {
  const [min, max] = Ranges.DIGITAL_OUTPUT;
  const positive = { channel: 2, value: 200, expected: '0201c8' };
  testUnsignedRange(positive, min, max, encoder.encodeDigitalOutput);
};

const testAnalogInput = function testAnalogInput() {
  const [min, max] = Ranges.ANALOG_INPUT;
  const negative = { channel: 3, value: -1.234, expected: '0302ff85' };
  const positive = { channel: 3, value: 1.234, expected: '0302007b' };
  testSignedRange(negative, positive, min, max, encoder.encodeAnalogInput);
};

const testAnalogOutput = function testAnalogOutput() {
  const [min, max] = Ranges.ANALOG_OUTPUT;
  const negative = { channel: 4, value: -1.234, expected: '0403ff85' };
  const positive = { channel: 4, value: 1.234, expected: '0403007b' };
  testSignedRange(negative, positive, min, max, encoder.encodeAnalogOutput);
};

const testLuminosity = function testLuminosity() {
  const [min, max] = Ranges.LUMINOSITY;
  const positive = { channel: 5, value: 12345, expected: '05653039' };
  testUnsignedRange(positive, min, max, encoder.encodeLuminosity);
};

const testPresence = function testPresence() {
  const [min, max] = Ranges.PRESENCE;
  const positive = { channel: 6, value: 1, expected: '066601' };
  testUnsignedRange(positive, min, max, encoder.encodePresence);
};

const testTemperature = function testTemperature() {
  const [min, max] = Ranges.TEMPERATURE;
  const negative = { channel: 7, value: -1234.5, expected: '0767cfc7' };
  const positive = { channel: 7, value: 1234.5, expected: '07673039' };
  testSignedRange(negative, positive, min, max, encoder.encodeTemperature);
};

const testRelativeHumidity = function testRelativeHumidity() {
  const [min, max] = Ranges.RELATIVE_HUMIDITY;
  const positive = { channel: 8, value: 40.75, expected: '086851' };
  testUnsignedRange(positive, min, max, encoder.encodeRelativeHumidity);
};

const testAccelerometer = function testAccelerometer() {
  const negative = {
    x: {
      channel: 9,
      values: { x: -1.234, y: 0.0, z: 0.0 },
      expected: '0971fb2e00000000',
    },
    y: {
      channel: 9,
      values: { x: 0.0, y: -1.234, z: 0.0 },
      expected: '09710000fb2e0000',
    },
    z: {
      channel: 9,
      values: { x: 0.0, y: 0.0, z: -1.234 },
      expected: '097100000000fb2e',
    },
  };
  const positive = {
    x: {
      channel: 9,
      values: { x: 1.234, y: 0.0, z: 0.0 },
      expected: '097104d200000000',
    },
    y: {
      channel: 9,
      values: { x: 0.0, y: 1.234, z: 0.0 },
      expected: '0971000004d20000',
    },
    z: {
      channel: 9,
      values: { x: 0.0, y: 0.0, z: 1.234 },
      expected: '09710000000004d2',
    },
  };
  const ranges = Object.keys(negative).reduce((a, c) => {
    /* eslint-disable-next-line no-param-reassign */
    a[c] = Ranges.ACCELEROMETER;
    return a;
  }, {});
  test3AxisRange(negative, positive, ranges, encoder.encodeAccelerometer);
};

const testMagnetometer = function testMagnetometer() {
  const negative = {
    x: {
      channel: 10,
      values: { x: -1.234, y: 0.0, z: 0.0 },
      expected: '0a72fb2e00000000',
    },
    y: {
      channel: 10,
      values: { x: 0.0, y: -1.234, z: 0.0 },
      expected: '0a720000fb2e0000',
    },
    z: {
      channel: 10,
      values: { x: 0.0, y: 0.0, z: -1.234 },
      expected: '0a7200000000fb2e',
    },
  };
  const positive = {
    x: {
      channel: 10,
      values: { x: 1.234, y: 0.0, z: 0.0 },
      expected: '0a7204d200000000',
    },
    y: {
      channel: 10,
      values: { x: 0.0, y: 1.234, z: 0.0 },
      expected: '0a72000004d20000',
    },
    z: {
      channel: 10,
      values: { x: 0.0, y: 0.0, z: 1.234 },
      expected: '0a720000000004d2',
    },
  };
  const ranges = Object.keys(negative).reduce((a, c) => {
    /* eslint-disable-next-line no-param-reassign */
    a[c] = Ranges.MAGNETOMETER;
    return a;
  }, {});
  test3AxisRange(negative, positive, ranges, encoder.encodeMagnetometer);
};

const testBarometricPressure = function testBarometricPressure() {
  const [min, max] = Ranges.BAROMETRIC_PRESSURE;
  const positive = { channel: 11, value: 1234.5, expected: '0b733039' };
  testUnsignedRange(positive, min, max, encoder.encodeBarometricPressure);
};

const testGyrometer = function testGyrometer() {
  const negative = {
    x: {
      channel: 12,
      values: { x: -1.234, y: 0.0, z: 0.0 },
      expected: '0c86ff8500000000',
    },
    y: {
      channel: 12,
      values: { x: 0.0, y: -1.234, z: 0.0 },
      expected: '0c860000ff850000',
    },
    z: {
      channel: 12,
      values: { x: 0.0, y: 0.0, z: -1.234 },
      expected: '0c8600000000ff85',
    },
  };
  const positive = {
    x: {
      channel: 12,
      values: { x: 1.234, y: 0.0, z: 0.0 },
      expected: '0c86007b00000000',
    },
    y: {
      channel: 12,
      values: { x: 0.0, y: 1.234, z: 0.0 },
      expected: '0c860000007b0000',
    },
    z: {
      channel: 12,
      values: { x: 0.0, y: 0.0, z: 1.234 },
      expected: '0c8600000000007b',
    },
  };
  const ranges = Object.keys(negative).reduce((a, c) => {
    /* eslint-disable-next-line no-param-reassign */
    a[c] = Ranges.GYROMETER;
    return a;
  }, {});
  test3AxisRange(negative, positive, ranges, encoder.encodeGyrometer);
};

const testGps = function testGps() {
  const negative = {
    latitude: {
      channel: 13,
      values: { latitude: -12.345, longitude: 0.0, altitude: 0.0 },
      expected: '0d88fe1dc6000000000000',
    },
    longitude: {
      channel: 13,
      values: { latitude: 0.0, longitude: -12.345, altitude: 0.0 },
      expected: '0d88000000fe1dc6000000',
    },
    altitude: {
      channel: 13,
      values: { latitude: 0.0, longitude: 0.0, altitude: -12.345 },
      expected: '0d88000000000000fffb2e',
    },
  };
  const positive = {
    latitude: {
      channel: 13,
      values: { latitude: 12.345, longitude: 0.0, altitude: 0.0 },
      expected: '0d8801e23a000000000000',
    },
    longitude: {
      channel: 13,
      values: { latitude: 0.0, longitude: 12.345, altitude: 0.0 },
      expected: '0d8800000001e23a000000',
    },
    altitude: {
      channel: 13,
      values: { latitude: 0.0, longitude: 0.0, altitude: 12.345 },
      expected: '0d880000000000000004d2',
    },
  };
  const ranges = {
    latitude: Ranges.GPS_LATITUDE,
    longitude: Ranges.GPS_LONGITUDE,
    altitude: Ranges.GPS_ALTITUDE,
  };
  test3AxisRange(negative, positive, ranges, encoder.encodeGps);
};

const execute = function execute() {
  describe('encodeDigitalInput()', () => {
    testDigitalInput();
  });

  describe('encodeDigitalOutput()', () => {
    testDigitalOutput();
  });

  describe('encodeAnalogInput()', () => {
    testAnalogInput();
  });

  describe('encodeAnalogOutput()', () => {
    testAnalogOutput();
  });

  describe('encodeLuminosity()', () => {
    testLuminosity();
  });

  describe('encodePresence()', () => {
    testPresence();
  });

  describe('encodeTemperature()', () => {
    testTemperature();
  });

  describe('encodeRelativeHumidity()', () => {
    testRelativeHumidity();
  });

  describe('encodeAccelerometer()', () => {
    testAccelerometer();
  });

  describe('encodeMagnetometer()', () => {
    testMagnetometer();
  });

  describe('encodeBarometricPressure()', () => {
    testBarometricPressure();
  });

  describe('encodeGyrometer()', () => {
    testGyrometer();
  });

  describe('encodeGps()', () => {
    testGps();
  });
};

module.exports.execute = execute;
