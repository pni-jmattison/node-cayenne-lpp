/* eslint-env mocha */

const { expect } = require('chai');
const { decoder } = require('../src');

const testDecode = function testDecode() {
  it('should decode temperature sensor data', () => {
    const buffer = Buffer.from('03670110056700ff0167ffd7', 'hex');
    const decoded = JSON.stringify(decoder.decode(buffer));
    const expected = '[{"temperature_3":27.2},{"temperature_5":25.5},{"temperature_1":-4.1}]';
    expect(decoded).to.equal(expected);
  });
  it('should decode accelerometer sensor data', () => {
    const buffer = Buffer.from('067104d2fb2e0000', 'hex');
    const decoded = JSON.stringify(decoder.decode(buffer));
    const expected = '[{"accelerometer_6":{"x":1.234,"y":-1.234,"z":0}}]';
    expect(decoded).to.equal(expected);
  });
  it('should decode GPS sensor data', () => {
    const buffer = Buffer.from('018806765ff2960a0003e8', 'hex');
    const decoded = JSON.stringify(decoder.decode(buffer));
    const expected = '[{"gps_1":{"latitude":42.3519,"longitude":-87.9094,"altitude":10}}]';
    expect(decoded).to.equal(expected);
  });
  it('should decode multiple events from the same sensor', () => {
    const buffer = Buffer.from('156601156600', 'hex');
    const decoded = JSON.stringify(decoder.decode(buffer));
    const expected = '[{"presence_21":1},{"presence_21":0}]';
    expect(decoded).to.equal(expected);
  });
};

const execute = function execute() {
  describe('decode()', () => {
    testDecode();
  });
};

module.exports.execute = execute;
