/* eslint-env mocha */

const encoderTest = require('./encoder');
const decoderTest = require('./decoder');

describe('Cayenne-LPP', () => {
  describe('Encoder', () => {
    encoderTest.execute();
  });
  describe('Decoder', () => {
    decoderTest.execute();
  });
});
