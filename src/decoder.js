const { Types, Sizes, Scales } = require('./datatypes');

const KEYS = Object.freeze({
  [Types.DIGITAL_INPUT]: 'digital_input',
  [Types.DIGITAL_OUTPUT]: 'digital_output',
  [Types.ANALOG_INPUT]: 'analog_input',
  [Types.ANALOG_OUTPUT]: 'analog_output',
  [Types.LUMINOSITY]: 'luminosity',
  [Types.PRESENCE]: 'presence',
  [Types.TEMPERATURE]: 'temperature',
  [Types.RELATIVE_HUMIDITY]: 'relative_humidity',
  [Types.ACCELEROMETER]: 'accelerometer',
  [Types.MAGNETOMETER]: 'magnetometer',
  [Types.BAROMETRIC_PRESSURE]: 'barometric_pressure',
  [Types.GYROMETER]: 'gyrometer',
  [Types.GPS]: 'gps',
});

const parseUInt8 = function parseUInt8(buffer, index, scale) {
  return buffer.readUInt8(index) / scale;
};

const parseInt16 = function parseInt16(buffer, index, scale) {
  return buffer.readInt16BE(index) / scale;
};

const parseUInt16 = function parseUInt16(buffer, index, scale) {
  return buffer.readUInt16BE(index) / scale;
};

const parse3Axis = function parse3Axis(buffer, index, scales) {
  let offset = 0;
  return ['x', 'y', 'z'].reduce((result, axis) => {
    /* eslint-disable-next-line no-param-reassign */
    result[axis] = buffer.readInt16BE(index + offset) / scales[axis];
    offset += 2;
    return result;
  }, {});
};

const parseDigitalInput = function parseDigitalInput(buffer, index) {
  return parseUInt8(buffer, index, Scales.DIGITAL_INPUT);
};

const parseDigitalOutput = function parseDigitalOutput(buffer, index) {
  return parseUInt8(buffer, index, Scales.DIGITAL_OUTPUT);
};

const parseAnalogInput = function parseAnalogInput(buffer, index) {
  return parseInt16(buffer, index, Scales.ANALOG_INPUT);
};

const parseAnalogOutput = function parseAnalogOutput(buffer, index) {
  return parseInt16(buffer, index, Scales.ANALOG_OUTPUT);
};

const parseLuminosity = function parseLuminosity(buffer, index) {
  return parseUInt16(buffer, index, Scales.LUMINOSITY);
};

const parsePresence = function parsePresence(buffer, index) {
  return parseUInt8(buffer, index, Scales.PRESENCE);
};

const parseTemperature = function parseTemperature(buffer, index) {
  return parseInt16(buffer, index, Scales.TEMPERATURE);
};

const parseRelativeHumidity = function parseRelativeHumidity(buffer, index) {
  return parseUInt8(buffer, index, Scales.RELATIVE_HUMIDITY);
};

const parseAccelometer = function parseAccelometer(buffer, index) {
  const scales = ['x', 'y', 'z'].reduce((result, axis) => {
    /* eslint-disable-next-line no-param-reassign */
    result[axis] = Scales.ACCELEROMETER;
    return result;
  }, {});
  return parse3Axis(buffer, index, scales);
};

const parseMagnetometer = function parseMagnetometer(buffer, index) {
  const scales = ['x', 'y', 'z'].reduce((result, axis) => {
    /* eslint-disable-next-line no-param-reassign */
    result[axis] = Scales.MAGNETOMETER;
    return result;
  }, {});
  return parse3Axis(buffer, index, scales);
};

const parseBarometricPressure = function parseBarometricPressure(
  buffer,
  index,
) {
  return parseInt16(buffer, index, Scales.BAROMETRIC_PRESSURE);
};

const parseGyrometer = function parseGyrometer(buffer, index) {
  const scales = ['x', 'y', 'z'].reduce((result, axis) => {
    /* eslint-disable-next-line no-param-reassign */
    result[axis] = Scales.GYROMETER;
    return result;
  }, {});
  return parse3Axis(buffer, index, scales);
};

/* eslint-disable no-bitwise */

const parseGPS = function parseGPS(buffer, index) {
  const scales = {
    latitude: Scales.GPS_LATITUDE,
    longitude: Scales.GPS_LONGITUDE,
    altitude: Scales.GPS_ALTITUDE,
  };
  let offset = 0;
  return ['latitude', 'longitude', 'altitude'].reduce((result, axis) => {
    let value = buffer.readInt8(index + offset) << 16;
    value |= buffer.readUInt8(index + offset + 1) << 8;
    value |= buffer.readUInt8(index + offset + 2);
    value /= scales[axis];
    /* eslint-disable-next-line no-param-reassign */
    result[axis] = value;
    offset += 3;
    return result;
  }, {});
};

/* eslint-enable no-bitwise */

const decode = function decode(buffer) {
  let index = 0;
  const result = [];
  while (index < buffer.length - 1) {
    const channel = buffer.readUInt8(index);
    index += 1;

    const sid = buffer.readUInt8(index);
    index += 1;

    const sensor = `${KEYS[sid]}_${channel}`;

    let value = null;

    switch (sid) {
      case Types.DIGITAL_INPUT:
        value = parseDigitalInput(buffer, index);
        index += Sizes.DIGITAL_INPUT;
        break;

      case Types.DIGITAL_OUTPUT:
        value = parseDigitalOutput(buffer, index);
        index += Sizes.DIGITAL_OUTPUT;
        break;

      case Types.ANALOG_INPUT:
        value = parseAnalogInput(buffer, index);
        index += Sizes.ANALOG_INPUT;
        break;

      case Types.ANALOG_OUTPUT:
        value = parseAnalogOutput(buffer, index);
        index += Sizes.ANALOG_OUTPUT;
        break;

      case Types.LUMINOSITY:
        value = parseLuminosity(buffer, index);
        index += Sizes.LUMINOSITY;
        break;

      case Types.PRESENCE:
        value = parsePresence(buffer, index);
        index += Sizes.PRESENCE;
        break;

      case Types.TEMPERATURE:
        value = parseTemperature(buffer, index);
        index += Sizes.TEMPERATURE;
        break;

      case Types.RELATIVE_HUMIDITY:
        value = parseRelativeHumidity(buffer, index);
        index += Sizes.RELATIVE_HUMIDITY;
        break;

      case Types.ACCELEROMETER:
        value = parseAccelometer(buffer, index);
        index += Sizes.ACCELEROMETER;
        break;

      case Types.MAGNETOMETER:
        value = parseMagnetometer(buffer, index);
        index += Sizes.MAGNETOMETER;
        break;

      case Types.BAROMETRIC_PRESSURE:
        value = parseBarometricPressure(buffer, index);
        index += Sizes.BAROMETRIC_PRESSURE;
        break;

      case Types.GYROMETER:
        value = parseGyrometer(buffer, index);
        index += Sizes.GYROMETER;
        break;

      case Types.GPS:
        value = parseGPS(buffer, index);
        index += Sizes.GPS;
        break;

      default:
        throw RangeError('Sensor type is out of range.');
    }
    result.push({ [sensor]: value });
  }
  return result;
};
module.exports.decode = decode;
