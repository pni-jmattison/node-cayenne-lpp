const {
  Ranges, Scales, Sizes, Types,
} = require('./datatypes');

/* eslint no-bitwise: "off" */

const rangeCheck = function rangeCheck(min, max, value) {
  if (value < min || value > max) {
    throw new RangeError(`The argument must be between ${min} and ${max}.`);
  }
};

const encodeChannelType = function encodeChannelType(channel, type) {
  const buffer = Buffer.allocUnsafe(Sizes.CHANNEL_TYPE);
  let offset = 0;
  [channel, type].forEach((value) => {
    offset = buffer.writeUInt8(value, offset);
  });
  return buffer;
};

const encodeUInt8 = function encodeUInt8(value, scale = 1) {
  const buffer = Buffer.allocUnsafe(Sizes.UINT8);
  buffer.writeUInt8((value * scale) | 0);
  return buffer;
};

const encodeUInt16 = function encodeUInt16(value, scale = 1) {
  const buffer = Buffer.allocUnsafe(Sizes.UINT16);
  buffer.writeUInt16BE((value * scale) | 0);
  return buffer;
};

const encodeInt16 = function encodeInt16(value, scale = 1) {
  const buffer = Buffer.allocUnsafe(Sizes.INT16);
  buffer.writeInt16BE((value * scale) | 0);
  return buffer;
};

const encode3Axis = function encode3Axis(x, y, z, scale = 1) {
  const buffer = Buffer.allocUnsafe(Sizes.THREE_AXIS);
  let offset = 0;
  [x, y, z].forEach((axis) => {
    offset = buffer.writeInt16BE((axis * scale) | 0, offset);
  });
  return buffer;
};

const encodeDigitalInput = function encodeDigitalInput(channel, value) {
  const [min, max] = Ranges.DIGITAL_INPUT;
  rangeCheck(min, max, value);
  const chanb = encodeChannelType(channel, Types.DIGITAL_INPUT);
  const snsb = encodeUInt8(value, Scales.DIGITAL_INPUT);
  return Buffer.concat([chanb, snsb], chanb.length + snsb.length);
};
module.exports.encodeDigitalInput = encodeDigitalInput;

const encodeDigitalOutput = function encodeDigitalOutput(channel, value) {
  const [min, max] = Ranges.DIGITAL_OUTPUT;
  rangeCheck(min, max, value);
  const chanb = encodeChannelType(channel, Types.DIGITAL_OUTPUT);
  const snsb = encodeUInt8(value, Scales.DIGITAL_OUTPUT);
  return Buffer.concat([chanb, snsb], chanb.length + snsb.length);
};
module.exports.encodeDigitalOutput = encodeDigitalOutput;

const encodeAnalogInput = function encodeAnalogInput(channel, value) {
  const [min, max] = Ranges.ANALOG_INPUT;
  rangeCheck(min, max, value);
  const chanb = encodeChannelType(channel, Types.ANALOG_INPUT);
  const snsb = encodeInt16(value, Scales.ANALOG_INPUT);
  return Buffer.concat([chanb, snsb], chanb.length + snsb.length);
};
module.exports.encodeAnalogInput = encodeAnalogInput;

const encodeAnalogOutput = function encodeAnalogOutput(channel, value) {
  const [min, max] = Ranges.ANALOG_OUTPUT;
  rangeCheck(min, max, value);
  const chanb = encodeChannelType(channel, Types.ANALOG_OUTPUT);
  const snsb = encodeInt16(value, Scales.ANALOG_OUTPUT);
  return Buffer.concat([chanb, snsb], chanb.length + snsb.length);
};
module.exports.encodeAnalogOutput = encodeAnalogOutput;

const encodeLuminosity = function encodeLuminosity(channel, lux) {
  const [min, max] = Ranges.LUMINOSITY;
  rangeCheck(min, max, lux);
  const chanb = encodeChannelType(channel, Types.LUMINOSITY);
  const snsb = encodeUInt16(lux, Scales.LUMINOSITY);
  return Buffer.concat([chanb, snsb], chanb.length + snsb.length);
};
module.exports.encodeLuminosity = encodeLuminosity;

const encodePresence = function encodePresence(channel, presence) {
  const [min, max] = Ranges.PRESENCE;
  rangeCheck(min, max, presence);
  const chanb = encodeChannelType(channel, Types.PRESENCE);
  const snsb = encodeUInt8(presence, Scales.PRESENCE);
  return Buffer.concat([chanb, snsb], chanb.length + snsb.length);
};
module.exports.encodePresence = encodePresence;

const encodeTemperature = function encodeTemperature(channel, degc) {
  const [min, max] = Ranges.TEMPERATURE;
  rangeCheck(min, max, degc);
  const chanb = encodeChannelType(channel, Types.TEMPERATURE);
  const snsb = encodeInt16(degc, Scales.TEMPERATURE);
  return Buffer.concat([chanb, snsb], chanb.length + snsb.length);
};
module.exports.encodeTemperature = encodeTemperature;

const encodeRelativeHumidity = function encodeRelativeHumidity(channel, rh) {
  const [min, max] = Ranges.RELATIVE_HUMIDITY;
  rangeCheck(min, max, rh);
  const chanb = encodeChannelType(channel, Types.RELATIVE_HUMIDITY);
  const snsb = encodeUInt8(rh, Scales.RELATIVE_HUMIDITY);
  return Buffer.concat([chanb, snsb], chanb.length + snsb.length);
};
module.exports.encodeRelativeHumidity = encodeRelativeHumidity;

const encodeAccelerometer = function encodeAccelerometer(channel, { x, y, z }) {
  const [min, max] = Ranges.ACCELEROMETER;
  [x, y, z].forEach((axis) => {
    rangeCheck(min, max, axis);
  });
  const chanb = encodeChannelType(channel, Types.ACCELEROMETER);
  const snsb = encode3Axis(x, y, z, Scales.ACCELEROMETER);
  return Buffer.concat([chanb, snsb], chanb.length + snsb.length);
};
module.exports.encodeAccelerometer = encodeAccelerometer;

const encodeMagnetometer = function encodeMagnetometer(channel, { x, y, z }) {
  const [min, max] = Ranges.MAGNETOMETER;
  [x, y, z].forEach((axis) => {
    rangeCheck(min, max, axis);
  });
  const chanb = encodeChannelType(channel, Types.MAGNETOMETER);
  const snsb = encode3Axis(x, y, z, Scales.MAGNETOMETER);
  return Buffer.concat([chanb, snsb], chanb.length + snsb.length);
};
module.exports.encodeMagnetometer = encodeMagnetometer;

const encodeBarometricPressure = function encodeBarometricPressure(
  channel,
  hpa,
) {
  const [min, max] = Ranges.BAROMETRIC_PRESSURE;
  rangeCheck(min, max, hpa);
  const chanb = encodeChannelType(channel, Types.BAROMETRIC_PRESSURE);
  const snsb = encodeUInt16(hpa, Scales.BAROMETRIC_PRESSURE);
  return Buffer.concat([chanb, snsb], chanb.length + snsb.length);
};
module.exports.encodeBarometricPressure = encodeBarometricPressure;

const encodeGyrometer = function encodeGyrometer(channel, { x, y, z }) {
  const [min, max] = Ranges.GYROMETER;
  [x, y, z].forEach((axis) => {
    rangeCheck(min, max, axis);
  });
  const chanb = encodeChannelType(channel, Types.GYROMETER);
  const snsb = encode3Axis(x, y, z, Scales.GYROMETER);
  return Buffer.concat([chanb, snsb], chanb.length + snsb.length);
};
module.exports.encodeGyrometer = encodeGyrometer;

const encodeGps = function encodeGps(
  channel,
  { latitude, longitude, altitude },
) {
  [
    [Ranges.GPS_LATITUDE, latitude],
    [Ranges.GPS_LONGITUDE, longitude],
    [Ranges.GPS_ALTITUDE, altitude],
  ].forEach((pair) => {
    const [[min, max], value] = pair;
    rangeCheck(min, max, value);
  });
  const chanb = encodeChannelType(channel, Types.GPS);
  const snsb = Buffer.allocUnsafe(Sizes.GPS);
  let offset = 0;
  [
    (latitude * Scales.GPS_LATITUDE) | 0,
    (longitude * Scales.GPS_LONGITUDE) | 0,
    (altitude * Scales.GPS_ALTITUDE) | 0,
  ].forEach((value) => {
    offset = snsb.writeUInt8((value >> 16) & 0xff, offset);
    offset = snsb.writeUInt8((value >> 8) & 0xff, offset);
    offset = snsb.writeUInt8(value & 0xff, offset);
  });
  return Buffer.concat([chanb, snsb], chanb.length + snsb.length);
};
module.exports.encodeGps = encodeGps;
