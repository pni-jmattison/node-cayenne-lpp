const { encoder } = require('../src');

const exampleTemperature = function exampleTemperature() {
  const data = Buffer.concat([
    encoder.encodeTemperature(3, 27.2),
    encoder.encodeTemperature(5, 25.5),
  ]);
  console.log(`Device with 2 temperature sensors: ${data.toString('hex')}`);
};

const exampleTemperatureAccelerometer = function exampleTemperatureAccelerometer() {
  console.log('Device with temperature and acceleration sensors');
  const temperature = encoder.encodeTemperature(1, -4.1);
  console.log(`- Frame N: ${temperature.toString('hex')}`);
  const accelerometer = encoder.encodeAccelerometer(6, {
    x: 1.234,
    y: -1.234,
    z: 0.0,
  });
  console.log(`- Frame N+1: ${accelerometer.toString('hex')}`);
};

const exampleGps = function exampleGps() {
  const gps = encoder.encodeGps(1, {
    latitude: 42.3519,
    longitude: -87.9094,
    altitude: 10.0,
  });
  console.log(`Device with GPS: ${gps.toString('hex')}`);
};

exampleTemperature();
exampleTemperatureAccelerometer();
exampleGps();
