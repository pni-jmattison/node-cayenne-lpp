const { decoder } = require('../src');

const exampleTemperature = function exampleTemperature() {
  const buffer = Buffer.from('03670110056700ff', 'hex');
  const json = JSON.stringify(decoder.decode(buffer));
  console.log(`Device with 2 temperature sensors: ${json}}`);
};

const exampleTemperatureAccelerometer = function exampleTemperatureAccelerometer() {
  const buffer = Buffer.from('0167ffd7067104d2fb2e0000', 'hex');
  const json = JSON.stringify(decoder.decode(buffer));
  console.log(`Device with temperature and acceleration sensors: ${json}}`);
};

const exampleGPS = function exampleGPS() {
  const buffer = Buffer.from('018806765ff2960a0003e8', 'hex');
  const json = JSON.stringify(decoder.decode(buffer));
  console.log(`Device with GPS: ${json}}`);
};

exampleTemperature();
exampleTemperatureAccelerometer();
exampleGPS();
